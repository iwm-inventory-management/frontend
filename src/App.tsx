import React from "react";
import { Route, Routes } from "react-router";
import Signin from "./components/Signin/Signin";
import Error from "./components/Error/Error";
import Ajustement from "./components/Ajustement/Ajustement";
import Stock from "./components/Stock/stock";
import Constitution from "./components/Constitution/Constitution";
import Mobile from "./components/Mobile/Mobile";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Stock />} />
        <Route path="/stock-location" element={<Mobile />} />
        <Route path="/ajustement" element={<Ajustement />} />
        <Route path="/constitution" element={<Constitution />} />
        <Route path="/stock" element={<Mobile />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </div>
  );
}

export default App;
