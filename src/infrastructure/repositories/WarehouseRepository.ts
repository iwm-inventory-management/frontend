import { Transaction } from './../../core/models/Transaction';
import { Location } from './../../core/models/Location';
import { Warehouse } from './../../core/models/Warehouse';
import { IWareHouseRepository } from './../../core/interfaces/IWarehouseRepository';
import { WarehouseAPI } from './../requests/warehouse.api';
import { Stock } from '../../core/models/Stock';

export class WareHouseRepository implements IWareHouseRepository {
    private warehouseAPI = new WarehouseAPI();

    async all() : Promise<Warehouse[]> {
        return (await this.warehouseAPI.all()).data.map((warehouse : Warehouse) => warehouse);
    }

    async allItems(warehouseId : string) : Promise<Stock[]> {
        return (await this.warehouseAPI.allItems(warehouseId)).data.map((item : Stock) => item);
    }

    async allLocations(warehouseId : string) : Promise<Location[]> {
        return (await this.warehouseAPI.allLocations(warehouseId)).data.map((location: Location) => location);
    }

    async allLocationItems(warehouseId: string, locationId: string) : Promise<Stock[]> {
        return (await this.warehouseAPI.allLocationItems(warehouseId, locationId)).data.map((item : Stock) => item);
    }

    async allItemTransaction(warehouseId: string, locationId: string, itemId: string) : Promise<Transaction[]> {
        return (await this.warehouseAPI.allItemTransaction(warehouseId, locationId, itemId)).data.map((transaction : Transaction) => transaction);
    }

    async ajustement(warehouseId: string, locationId: string, itemId: string, transaction: Transaction) : Promise<any> {
        return (await this.warehouseAPI.ajustementStock(warehouseId, locationId, itemId, transaction))
    }
}