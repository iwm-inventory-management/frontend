import { Transaction } from './../../core/models/Transaction';
import request from "../../helpers/request"

export class WarehouseAPI {
    async all() {
        return request.get("warehouses");
    }

    async allItems(warehouseId : string) {
        return request.get(`warehouses/${warehouseId}/stocks`)
    }

    async allLocations(warehouseId: string) {
        return request.get(`warehouses/${warehouseId}/locations`);
    }

    async allLocationItems(warehouseId: string, locationId: string) {
        return request.get(`warehouses/${warehouseId}/locations/${locationId}/stocks`);
    }

    async allItemTransaction(warehouseId: string, locationId: string, itemId: string) {
        return request.get(`warehouses/${warehouseId}/locations/${locationId}/products/${itemId}/transactions`);
    }

    async ajustementStock(warehouseId: string, locationId: string, itemId: string, body: Transaction) {
        return request.post(`warehouses/${warehouseId}/locations/${locationId}/products/${itemId}/transactions`, body);
    }
}