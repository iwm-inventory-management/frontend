import { QueryClient } from 'react-query';
import { IWareHouseService } from './core/interfaces/IWarehouseService';
import { WarehouseService } from './core/services/WarehouseService';
import { WareHouseRepository } from './infrastructure/repositories/WarehouseRepository';

export const queryClient = new QueryClient();

const wareHouseRepository = new WareHouseRepository();

export const warehouseService: IWareHouseService = new WarehouseService(
    wareHouseRepository
);
