export interface Stock {
    warehouse: string;
    location: string;
    article: string;
    quantity: number;
}