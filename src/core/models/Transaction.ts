export interface Transaction {
    type: string;
    product: string;
    quantity: number;
    warehouse: string;
    location: string;
    user: string;
    date: string;
}