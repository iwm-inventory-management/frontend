import { Transaction } from './../models/Transaction';
import { IWareHouseService } from './../interfaces/IWarehouseService';
import { Warehouse } from '../models/Warehouse';
import { IWareHouseRepository } from './../interfaces/IWarehouseRepository';
import { Stock } from '../models/Stock';
import { Location } from "../models/Location"

export class WarehouseService implements IWareHouseService{
    constructor(private wareHouseRepository: IWareHouseRepository) {}

    async getAllWarehouses() : Promise<Warehouse[]> {
        return await this.wareHouseRepository.all();
    }

    async getWarehouseItems(warehouseId : string) : Promise<Stock[]> {
        return await this.wareHouseRepository.allItems(warehouseId);
    }

    async getWarehouseLocations(warehouseId : string) : Promise<Location[]> {
        return await this.wareHouseRepository.allLocations(warehouseId);
    }

    async getWarehouseLocationItems(warehouseId: string, locationId: string) : Promise<Stock[]> {
        return await this.wareHouseRepository.allLocationItems(warehouseId, locationId);
    }

    async getWarehouseItemTransactions(warehouseId: string, locationId: string, itemId: string) : Promise<Transaction[]> {
        return await this.wareHouseRepository.allItemTransaction(warehouseId, locationId, itemId);
    }

    async addTransaction(warehouseId: string, locationId: string, itemId: string, transaction: Transaction) : Promise<any> {

        if(warehouseId === "" || locationId === "" || itemId === "") {
            throw new Error(
                "Toutes les champs doivent être remplies"
            );
        }

        await this.wareHouseRepository.ajustement(warehouseId, locationId, itemId, transaction);

        return transaction;
    }
}