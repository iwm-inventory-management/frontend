import { Transaction } from './../models/Transaction';
import { Stock } from "../models/Stock";
import { Warehouse } from "../models/Warehouse";
import { Location } from "../models/Location"


export interface IWareHouseRepository {
    all() : Promise<Warehouse[]>;
    allItems(warehouseId : string) : Promise<Stock[]>;
    allLocations(warehouseId: string) : Promise<Location[]>;
    allLocationItems(warehouseId: string, locationId: string) : Promise<Stock[]>
    allItemTransaction(warehouseId: string, locationId: string, itemId: string) : Promise<Transaction[]>
    ajustement(warehouseId: string, locationId: string, itemId: string, transaction: Transaction) : Promise<any>
}