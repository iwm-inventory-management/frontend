import { Transaction } from './../models/Transaction';
import { Stock } from "../models/Stock";
import { Warehouse } from "../models/Warehouse";
import { Location } from "../models/Location"

export interface IWareHouseService {
    getAllWarehouses() : Promise<Warehouse[]>
    getWarehouseItems(warehouseId : string): Promise<Stock[]>
    getWarehouseLocations(warehouseId : string) : Promise<Location[]>
    getWarehouseLocationItems(warehouseId: string, locationId: string) : Promise<Stock[]>
    getWarehouseItemTransactions(warehouseId: string, locationId: string, itemId: string) : Promise<Transaction[]>
    addTransaction(warehouseId: string, locationId: string, itemId: string, transaction: Transaction) : Promise<any>
}