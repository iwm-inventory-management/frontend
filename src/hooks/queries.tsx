import { useQuery } from "react-query";
import { queryClient, warehouseService } from "../global";

export function useAllWarehouses() {
  return useQuery(
    ["warehouse"],
    async () => await warehouseService.getAllWarehouses()
  );
}

export function useWarehouseItems(warehouseId: string | undefined) {
  return useQuery(
    ["warehouseItem"],
    async () => await warehouseService.getWarehouseItems(warehouseId ?? "")
  );
}

export function invalideQueries() {
  queryClient.invalidateQueries();
}
