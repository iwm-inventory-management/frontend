import { useEffect, useState } from "react";
import { Button, Form, Table } from "react-bootstrap";
import { Stock } from "../../core/models/Stock";
import { Location } from "../../core/models/Location";
import { warehouseService } from "../../global";
import { useAllWarehouses } from "../../hooks/queries";
import { Transaction } from "../../core/models/Transaction";
import Menu from "../Menu/Menu";

const Constitution = () => {
  const allWarehouse = useAllWarehouses();

  const [allLocations, setAllLocations] = useState<Location[]>([]);

  const [allItems, setAllItems] = useState<Stock[]>([]);

  const [currentWarehouseId, setCurrentWarehouseId] = useState<string>("");

  const [currentLocationId, setCurrentLocationId] = useState<string>("");

  const [currentItemId, setCurrentItemId] = useState<string>("");

  const [allItemsTransaction, setAllItemsTransaction] = useState<Transaction[]>(
    []
  );

  const getWarehousesOptions = () => {
    if (allWarehouse.data) {
      return allWarehouse.data.map((warehouse) => (
        <option value={warehouse.id} key={warehouse.id}>
          {warehouse.id}
        </option>
      ));
    }
  };

  const getLocationsOptions = () => {
    if (allLocations) {
      return allLocations.map((location) => (
        <option value={location.id} key={location.id}>
          {location.id}
        </option>
      ));
    }
  };

  const getItemsOptions = () => {
    if (allItems) {
      return allItems.map((item) => (
        <option value={item.article} key={item.article}>
          {item.article}
        </option>
      ));
    }
  };

  const onChangeOptionWarehouse = (val: any) => {
    setCurrentWarehouseId(val.target.value);
    setCurrentLocationId("");
    setCurrentItemId("");
  };

  const onChangeOptionLocation = (val: any) => {
    setCurrentLocationId(val.target.value);
    setCurrentItemId("");
  };

  const onChangeOptionItem = (val: any) => {
    setCurrentItemId(val.target.value);
  };

  useEffect(() => {
    const getWarehouseLocations = async () => {
      warehouseService
        .getWarehouseLocations(currentWarehouseId)
        .then((data) => {
          setAllLocations(data);
        });
    };

    const getWarehouseItems = async () => {
      warehouseService
        .getWarehouseLocationItems(currentWarehouseId, currentLocationId)
        .then((data) => {
          setAllItems(data);
        });
    };

    const getAllItemsTransaction = async () => {
      warehouseService
        .getWarehouseItemTransactions(
          currentWarehouseId,
          currentLocationId,
          currentItemId
        )
        .then((data) => {
          setAllItemsTransaction(data);
        });
    };

    const test = async () => {
      if (currentWarehouseId !== "") {
        await getWarehouseLocations();

        if (currentLocationId !== "") {
          await getWarehouseItems();

          if (currentItemId !== "") {
            await getAllItemsTransaction();
          }
        }
      }
    };

    test().catch((err) => console.log(err));
  }, [
    currentWarehouseId,
    currentLocationId,
    currentItemId,
    allItemsTransaction,
  ]);

  return (
    <>
      <Menu />
      <h1>Recherche</h1>
      <Form>
        <Form.Group className="mb-3">
          <Form.Label>Warehouse</Form.Label>
          <Form.Select
            value={currentWarehouseId}
            onChange={(e) => onChangeOptionWarehouse(e)}
          >
            <option value="">Choisir un entrepôt</option>
            {getWarehousesOptions()}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Location</Form.Label>
          <Form.Select
            value={currentLocationId}
            onChange={(e) => onChangeOptionLocation(e)}
          >
            <option value="">Choisir un location</option>
            {getLocationsOptions()}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Item</Form.Label>
          <Form.Select
            value={currentItemId}
            onChange={(e) => onChangeOptionItem(e)}
          >
            <option value="">Choisir un item</option>
            {getItemsOptions()}
          </Form.Select>
        </Form.Group>
      </Form>
      {currentWarehouseId &&
        currentLocationId &&
        currentItemId &&
        allItemsTransaction.length !== 0 && (
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Date</th>
                <th>Quantity</th>
                <th>Type</th>
              </tr>
            </thead>
            <tbody>
              {allItemsTransaction.map((transaction, i) => (
                <tr key={i} style={{ cursor: "pointer" }}>
                  <td>{transaction.date}</td>
                  <td>{transaction.quantity}</td>
                  <td>{transaction.type}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
    </>
  );
};

export default Constitution;
