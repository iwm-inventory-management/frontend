import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { warehouseService } from "../../global";
import { useAllWarehouses } from "../../hooks/queries";
import { Location } from "../../core/models/Location";
import { Stock } from "../../core/models/Stock";
import Menu from "../Menu/Menu";

const Ajustement = () => {
  const allWarehouse = useAllWarehouses();

  const [allLocations, setAllLocations] = useState<Location[]>([]);

  const [allItems, setAllItems] = useState<Stock[]>([]);

  const [currentWarehouseId, setCurrentWarehouseId] = useState<string>("");

  const [currentLocationId, setCurrentLocationId] = useState<string>("");

  const [currentItemId, setCurrentItemId] = useState<string>("");

  const [showAjustement, setShowAjustement] = useState<boolean>(true);

  const [ajustementQuantity, setAjustementQuantity] = useState<number>(0);

  const getWarehousesOptions = () => {
    if (allWarehouse.data) {
      return allWarehouse.data.map((warehouse) => (
        <option value={warehouse.id} key={warehouse.id}>
          {warehouse.id}
        </option>
      ));
    }
  };

  const getLocationsOptions = () => {
    if (allLocations) {
      return allLocations.map((location) => (
        <option value={location.id} key={location.id}>
          {location.id}
        </option>
      ));
    }
  };

  const getItemsOptions = () => {
    if (allItems) {
      return allItems.map((item) => (
        <option value={item.article} key={item.article}>
          {item.article}
        </option>
      ));
    }
  };

  const onChangeOptionWarehouse = (val: any) => {
    setCurrentWarehouseId(val.target.value);
    setCurrentLocationId("");
    setCurrentItemId("");
  };

  const onChangeOptionLocation = (val: any) => {
    setCurrentLocationId(val.target.value);
    setCurrentItemId("");
  };

  const onChangeOptionItem = (val: any) => {
    setCurrentItemId(val.target.value);

    if (val.target.value !== "") {
      setShowAjustement(false);
    } else {
      setShowAjustement(true);
    }
  };

  const onButtonClick = () => {
    const date = new Date().toISOString().slice(0, -5);

    const transaction = {
      type: "Adjust",
      product: currentItemId,
      quantity: ajustementQuantity,
      warehouse: currentWarehouseId,
      location: currentLocationId,
      user: "toto",
      date,
    };

    warehouseService.addTransaction(
      currentWarehouseId,
      currentLocationId,
      currentItemId,
      transaction
    );
  };

  useEffect(() => {
    const getWarehouseLocations = async () => {
      warehouseService
        .getWarehouseLocations(currentWarehouseId)
        .then((data) => {
          setAllLocations(data);
        });
    };

    const getWarehouseItems = async () => {
      warehouseService
        .getWarehouseLocationItems(currentWarehouseId, currentLocationId)
        .then((data) => {
          setAllItems(data);
        });
    };

    const test = async () => {
      if (currentWarehouseId !== "") {
        await getWarehouseLocations();

        if (currentLocationId !== "") {
          await getWarehouseItems();
        }
      }
    };

    test().catch((err) => console.log(err));
  }, [currentWarehouseId, currentLocationId, currentItemId]);

  return (
    <>
      <Menu />
      <h1>Recherche</h1>
      <Form>
        <Form.Group className="mb-3">
          <Form.Label>Warehouse</Form.Label>
          <Form.Select
            value={currentWarehouseId}
            onChange={(e) => onChangeOptionWarehouse(e)}
          >
            <option value="">Choisir un entrepôt</option>
            {getWarehousesOptions()}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Location</Form.Label>
          <Form.Select
            value={currentLocationId}
            onChange={(e) => onChangeOptionLocation(e)}
          >
            <option value="">Choisir un location</option>
            {getLocationsOptions()}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Item</Form.Label>
          <Form.Select
            value={currentItemId}
            onChange={(e) => onChangeOptionItem(e)}
          >
            <option value="">Choisir un item</option>
            {getItemsOptions()}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3" hidden={showAjustement}>
          <Form.Label>Ajustement du stock</Form.Label>
          <Form.Control
            type="text"
            placeholder="Rentrez une référence"
            onChange={(e) => setAjustementQuantity(Number(e.target.value))}
          />
          <Button variant="outline-primary" onClick={onButtonClick}>
            Ajuster
          </Button>{" "}
        </Form.Group>
      </Form>
    </>
  );
};

export default Ajustement;
