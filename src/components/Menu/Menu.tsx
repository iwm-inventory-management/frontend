import { Container, Nav, Navbar } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";

const Menu = () => {
  return (
    <Navbar bg="primary" variant="dark">
      <Container>
        <Navbar.Brand href="#home">Mamazon</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link as={Link} to="/">
            Stock
          </Nav.Link>
          <Nav.Link as={Link} to="/stock-location">
            Stock location
          </Nav.Link>
          <Nav.Link as={Link} to="/ajustement">
            Ajustement
          </Nav.Link>
          <Nav.Link as={Link} to="/constitution">
            Constitution stock
          </Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default Menu;
