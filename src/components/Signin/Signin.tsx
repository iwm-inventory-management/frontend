import { Button, Form } from "react-bootstrap";

const Signin = () => {
  return (
    <>
        <Form id="loginForm">
          <h1>Connexion</h1>
          <Form.Group className="mb-3">
            <Form.Label>Identifiant</Form.Label>
            <Form.Control type="email" placeholder="Adresse mail" />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Mot de passe" />
          </Form.Group>
          <Button variant="primary" type="submit">
            Connexion
          </Button>
        </Form>
    </>
  );
};

export default Signin;
