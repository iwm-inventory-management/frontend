import { useEffect, useState } from "react";
import { Button, Form, Table } from "react-bootstrap";
import { warehouseService } from "../../global";
import { useAllWarehouses } from "../../hooks/queries";
import { Location } from "../../core/models/Location";
import { Stock } from "../../core/models/Stock";
import Menu from "../Menu/Menu";

const Mobile = () => {
  const allWarehouse = useAllWarehouses();

  const [allLocations, setAllLocations] = useState<Location[]>([]);

  const [allItems, setAllItems] = useState<Stock[]>([]);

  const [currentWarehouseId, setCurrentWarehouseId] = useState<string>("");

  const [currentLocationId, setCurrentLocationId] = useState<string>("");

  const [currentItemId, setCurrentItemId] = useState<string>("");

  const getWarehousesOptions = () => {
    if (allWarehouse.data) {
      return allWarehouse.data.map((warehouse) => (
        <option value={warehouse.id} key={warehouse.id}>
          {warehouse.id}
        </option>
      ));
    }
  };

  const getLocationsOptions = () => {
    if (allLocations) {
      return allLocations.map((location) => (
        <option value={location.id} key={location.id}>
          {location.id}
        </option>
      ));
    }
  };

  const getItemsOptions = () => {
    if (allItems) {
      return allItems.map((item) => (
        <option value={item.article} key={item.article}>
          {item.article}
        </option>
      ));
    }
  };

  const onChangeOptionWarehouse = (val: any) => {
    setCurrentWarehouseId(val.target.value);
    setCurrentLocationId("");
    setCurrentItemId("");
  };

  const onChangeOptionLocation = (val: any) => {
    setCurrentLocationId(val.target.value);
    setCurrentItemId("");
  };

  useEffect(() => {
    const getWarehouseLocations = async () => {
      warehouseService
        .getWarehouseLocations(currentWarehouseId)
        .then((data) => {
          setAllLocations(data);
        });
    };

    const getWarehouseItems = async () => {
      warehouseService
        .getWarehouseLocationItems(currentWarehouseId, currentLocationId)
        .then((data) => {
          setAllItems(data);
        });
    };

    const test = async () => {
      if (currentWarehouseId !== "") {
        await getWarehouseLocations();

        if (currentLocationId !== "") {
          await getWarehouseItems();
        }
      }
    };

    test().catch((err) => console.log(err));
  }, [currentWarehouseId, currentLocationId, currentItemId]);

  return (
    <>
      <Menu />
      <h1>Recherche</h1>
      <Form>
        <Form.Group className="mb-3">
          <Form.Label>Warehouse</Form.Label>
          <Form.Select
            value={currentWarehouseId}
            onChange={(e) => onChangeOptionWarehouse(e)}
          >
            <option value="">Choisir un entrepôt</option>
            {getWarehousesOptions()}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Location</Form.Label>
          <Form.Select
            value={currentLocationId}
            onChange={(e) => onChangeOptionLocation(e)}
          >
            <option value="">Choisir un location</option>
            {getLocationsOptions()}
          </Form.Select>
        </Form.Group>
      </Form>
      {currentWarehouseId && currentLocationId && allItems.length !== 0 && (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Article</th>
              <th>Location</th>
              <th>Stock</th>
            </tr>
          </thead>
          <tbody>
            {allItems.map((stock, i) => (
              <tr key={i} style={{ cursor: "pointer" }}>
                <td>{stock.article}</td>
                <td>{stock.location}</td>
                <td>{stock.quantity}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </>
  );
};

export default Mobile;
