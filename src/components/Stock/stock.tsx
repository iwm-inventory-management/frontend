import { Button, Form, Table } from "react-bootstrap";
import {
  faS,
  faClipboardList,
  faPenToSquare,
} from "@fortawesome/free-solid-svg-icons";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { warehouseService } from "../../global";
import { useAllWarehouses } from "../../hooks/queries";
import { Stock as StockItem } from "../../core/models/Stock";
import Menu from "../Menu/Menu";
library.add(faS, faClipboardList, faPenToSquare);

const Stock = () => {
  const allWarehouse = useAllWarehouses();

  const [currentWarehouseId, setCurrentWarehouseId] = useState<string>("");

  const [currentStock, setCurrentStock] = useState<StockItem[]>([]);

  const [filter, setFilter] = useState<string>("");

  const getOptions = () => {
    if (allWarehouse.data) {
      return allWarehouse.data.map((warehouse) => (
        <option value={warehouse.id} key={warehouse.id}>
          {warehouse.id}
        </option>
      ));
    }
  };

  const onChangeOption = (val: any) => {
    setCurrentWarehouseId(val.target.value);
  };

  useEffect(() => {
    if (currentWarehouseId !== "") {
      warehouseService.getWarehouseItems(currentWarehouseId).then((data) => {
        let filteredStock = data.filter((item) =>
          item.article.toLowerCase().includes(filter.toLowerCase())
        );
        setCurrentStock(filteredStock);
      });
    } else {
      setCurrentStock([]);
    }
  }, [currentWarehouseId, filter]);

  return (
    <>
      <Menu />
      <h1>Recherche</h1>
      <Form>
        <Form.Group className="mb-3">
          <Form.Label>Warehouse</Form.Label>
          <Form.Select onChange={onChangeOption}>
            <option value="">Choisir un entrepôt</option>
            {getOptions()}
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Item</Form.Label>
          <Form.Control
            type="text"
            placeholder="Rentrez une référence"
            onChange={(e) => setFilter(e.target.value)}
          />
        </Form.Group>
      </Form>
      {currentStock.length !== 0 && (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Article</th>
              <th>Location</th>
              <th>Stock</th>
            </tr>
          </thead>
          <tbody>
            {currentStock.map((stock, i) => (
              <tr key={i} style={{ cursor: "pointer" }}>
                <td>{stock.article}</td>
                <td>{stock.location}</td>
                <td>{stock.quantity}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </>
  );
};

export default Stock;
