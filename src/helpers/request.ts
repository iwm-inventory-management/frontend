import axios from "axios";

const request = axios.create({
    //baseURL: process.env["REACT_APP_API_URL"],
    baseURL: "https://stock-api.ezyran.xyz/api/"
});

request.defaults.headers.post["Content-Type"] = "application/json" // ou application/xml

export default request;

